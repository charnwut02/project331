import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule
} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { StudentsRestImplService } from './service/students-rest-impl.service';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { UserService } from './service/user.service';
import { UserFileImplService } from './service/user-file-impl.service';
import { AddComponent } from './activity/add/add.component';
import { ListComponent } from './activity/list/list.component';
import { EditComponent } from './activity/edit/edit.component';
 import { ActivityRoutingModule } from './activity/activity-routing.module';
import { ActivityService } from './service/activity.service';
import { ActivityRestImplService } from './service/activity-rest-impl.service';


import { ActivityTableComponent } from './activity/activity-table/activity-table.component';



@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    AddComponent,
    ListComponent,
    EditComponent,
    ActivityTableComponent,
   
    
    
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    StudentRoutingModule,
    ActivityRoutingModule,
    AppRoutingModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentsRestImplService },
    { provide: UserService, useClass: UserFileImplService },
    { provide: ActivityService, useClass: ActivityRestImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
