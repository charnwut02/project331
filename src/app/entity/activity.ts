export default class Activity {
    id: number;
    actId: string;
    actName: string;
    date: String;
    location: string;
    period: string;
    description: string;
    actHost: string;
}
