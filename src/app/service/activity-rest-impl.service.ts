import { Injectable } from '@angular/core';
import { ActivityService } from './activity.service';
import { HttpClient } from '@angular/common/http';
import { StudentsRestImplService } from './students-rest-impl.service';
import Activity  from '../entity/activity';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActivityRestImplService implements ActivityService{
  
  getActivitys(): Observable<Activity[]> {
    
    throw new Error("Method not implemented.");
  }
  getActivity(id: number): Observable<Activity> {
    throw new Error("Method not implemented.");
  }

 
  constructor(private http:HttpClient,private student:StudentsRestImplService) {
   }

   saveActivity(activity: Activity): Observable<Activity>{
    this.activity.push(activity);
    return of(activity);
   }

  activity : Activity[] = [
    {
      "id":1,
      "actId": "a-001",
      "actName":"pingpong",
      "date":"12/12/18",
      "location":"camt",
      "period":"12:00-13:00",
      "description":"fun activity",
      "actHost":"aj. nui"
    }
  ]
}

