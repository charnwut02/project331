import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentsRestImplService extends StudentService {

  constructor(private http: HttpClient) {
    super();
  }

  getStudents(): Observable<Student[]> {
    return of(this.student);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, student);
  }
  student:Student[] =[{
  id: 1,
  studentId: "592115009",
  name: "Charnwut",
  surname: "Thopurin",
  image: "",
  },{
    id: 2,
    studentId: "592115003",
    name: "Kittinut",
    surname: "Saengsri",
    image: "",
  },{
    id: 3,
    studentId: "592115024",
    name: "Phumsiri",
    surname: "Jiraponsawad",
    image: "",
  }];
    
  
}
