import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import User from '../entity/user';

@Injectable({
  providedIn: 'root'
})
export abstract class UserService {

  abstract getUsers(): Observable<User[]>;
}
