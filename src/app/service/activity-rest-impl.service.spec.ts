import { TestBed, inject } from '@angular/core/testing';

import { ActivityRestImplService } from './activity-rest-impl.service';

describe('ActivityRestImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityRestImplService]
    });
  });

  it('should be created', inject([ActivityRestImplService], (service: ActivityRestImplService) => {
    expect(service).toBeTruthy();
  }));
});
