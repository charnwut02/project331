import { TestBed, inject } from '@angular/core/testing';

import { UserFileImplService } from './user-file-impl.service';

describe('UserFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserFileImplService]
    });
  });

  it('should be created', inject([UserFileImplService], (service: UserFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
