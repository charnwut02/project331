import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import User from '../entity/user'
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class UserFileImplService extends UserService {

  constructor(private http:HttpClient) {
    super();
   }

   getUsers(): Observable<User[]> {
    return this.http.get<User[]> ('assets/user.json');
  }
}
