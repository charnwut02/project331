import { Component, OnInit } from '@angular/core';
import User from '../entity/user';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users: User[];
  username: string;
  password: string;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  login(): void {
    let isValid = false;
    if (Array.isArray(this.users)) {
      for (const user of this.users) {
        if (this.username === user.username && this.password === user.password) {
          isValid = true;
        }
      }

      if (isValid) {
        this.router.navigate(['home']);
      } else {
        alert("Invalid");
      }
    } else {
      return null;
    }
  }
  
}

