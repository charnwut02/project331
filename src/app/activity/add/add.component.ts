import { Component, OnInit } from '@angular/core';
import Activity  from 'src/app/entity/activity';
import { Router } from '@angular/router';
import { ActivityService } from '../../service/activity.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {
  model: Activity = new Activity();

  constructor(private activityService: ActivityService, private router: Router) { }

  onSubmit() {
    this.activityService.saveActivity(this.model)
      .subscribe((activity) => {
        this.router.navigate(['/detail', activity.actId]);
      }, (error) => {
        alert('could not save value');
      });
  }
  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }
}

