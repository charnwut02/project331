import { Component, OnInit } from '@angular/core';
import { ActivityService } from 'src/app/service/activity.service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  activity: Activity[];

  constructor(private activityService: ActivityService) { }
  ngOnInit() {
  //  this.activityService.getActivity()
    //  .subscribe(this.activity => this.activity = this.activity);
  }

}
